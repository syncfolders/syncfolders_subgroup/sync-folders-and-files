# Initial configuration - default paths
$sourceDir = "C:\Source"
$replicaDir = "C:\Replica"
$logFile = "C:\Logs\sync.log"

function Set-SourceDirectory {
    Write-Host "Enter the path of the Source Directory:"
    $global:sourceDir = Read-Host "Source Directory"
}

function Set-ReplicaDirectory {
    Write-Host "Enter the path of the Replica Directory:"
    $global:replicaDir = Read-Host "Replica Directory"
}

function Set-LogFile {
    Write-Host "Enter the path of the Log File:"
    $global:logFile = Read-Host "Log File"
}

function Show-CurrentSettings {
    Write-Host "Current Settings:" -ForegroundColor Cyan
    Write-Host "Source Directory: $global:sourceDir"
    Write-Host "Replica Directory: $global:replicaDir"
    Write-Host "Log File: $global:logFile"
    Write-Host "Press Enter to continue..."
    Read-Host
}

function Run-Synchronization {
    .\folder_sync.ps1 -sourceDir $global:sourceDir -replicaDir $global:replicaDir -logFile $global:logFile
    Write-Host "Synchronization completed. Press Enter to continue..." -ForegroundColor Green
    Read-Host "Press Enter to continue..."
}

function Show-Menu {
    param (
        [string]$Title = 'Synchronization Script Menu'
    )
    Clear-Host
    Write-Host "================ $Title ================"

    Write-Host "1: Run Synchronization"
    Write-Host "2: Set Source Directory"
    Write-Host "3: Set Replica Directory"
    Write-Host "4: Set Log File Path"
    Write-Host "5: Show Current Settings"
    Write-Host "Q: Exit"
}

do {
    Show-Menu
    $input = Read-Host "Please make a selection"
    switch ($input) {
        '1' {
            Run-Synchronization
        }
        '2' {
            Set-SourceDirectory
        }
        '3' {
            Set-ReplicaDirectory
        }
        '4' {
            Set-LogFile
        }
        '5' {
            Show-CurrentSettings
        }
        'Q' {
            return
        }
        default {
            Write-Host "Invalid selection, please try again" -ForegroundColor Red
        }
    }
    pause
}
while ($input -ne 'Q')
