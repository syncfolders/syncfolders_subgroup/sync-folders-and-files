param(
    [string]$sourceDir,
    [string]$replicaDir,
    [string]$logFile
)

function Write-Log {
    param(
        [string]$message,
        [string]$level = "INFO"
    )
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    $logMessage = "$timestamp [$level] $message"
    Add-Content -Value $logMessage -Path $logFile
    Write-Host $logMessage
}

function Sync-Folders {
    try {
        # Check if source directory exists
        if (-not (Test-Path $sourceDir)) {
            Write-Log "Error: Source directory '$sourceDir' does not exist" -level "ERROR"
            return
        }

        # Create replica directory if it does not exist
        if (-not (Test-Path $replicaDir)) {
            New-Item -Path $replicaDir -ItemType Directory | Out-Null
            Write-Log "Created replica directory: $replicaDir"
        }

        # Synchronize files and directories from source to replica
        $sourceItems = Get-ChildItem -Path $sourceDir -Recurse
        foreach ($item in $sourceItems) {
            $relativePath = $item.FullName.Substring($sourceDir.Length).TrimStart('\')
            $targetPath = Join-Path -Path $replicaDir -ChildPath $relativePath

            if ($item -is [System.IO.DirectoryInfo]) {
                if (-not (Test-Path $targetPath)) {
                    New-Item -Path $targetPath -ItemType Directory | Out-Null
                    Write-Log "Created directory: $targetPath"
                }
            } elseif ($item -is [System.IO.FileInfo]) {
                if (-not (Test-Path $targetPath) -or $item.LastWriteTime -gt (Get-Item $targetPath).LastWriteTime) {
                    Copy-Item -Path $item.FullName -Destination $targetPath -Force
                    Write-Log "Copied/Updated: $targetPath"
                }
            }
        }

       # Remove items in replica not in source
		$replicaItems = Get-ChildItem -Path $replicaDir -Recurse | Sort-Object FullName -Descending
		foreach ($item in $replicaItems) {
			$relativePath = $item.FullName.Substring($replicaDir.Length).TrimStart('\')
			$sourcePath = Join-Path -Path $sourceDir -ChildPath $relativePath

			if (-not (Test-Path $sourcePath)) {
				Remove-Item -Path $item.FullName -Force -Recurse
				Write-Log "Removed: $($item.FullName)"
			}
		}


    } catch {
        Write-Log "Error: $_" -level "ERROR"
    }
}

# Execute synchronization
Write-Log "Starting synchronization from '$sourceDir' to '$replicaDir'"
Sync-Folders
Write-Log "Synchronization completed"
