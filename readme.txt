# PowerShell Synchronization Script README

## Overview

This PowerShell script provides a robust solution for synchronizing the contents of a source directory to a replica directory, ensuring that the replica is an identical copy of the source. It is designed for flexibility, allowing users to specify the paths of the source and replica directories, as well as the log file, through a simple user interface.

## Features

- **One-way Synchronization**: Mirrors the source directory to the replica, including copying new or updated files, creating needed directories, and removing items that no longer exist in the source.
- **Logging**: Logs operations to a file and the console, including file copying, updates, and deletions.
- **User Interface**: Provides a simple menu-driven interface for setting up synchronization parameters.

## How to Use

1. **Set Directories and Log File**: Run the UI script ( Sync_Menu.ps1) to configure the source and replica directories, and specify the log file path.

2. **Run Synchronization**: Choose the "Run Synchronization" option from the menu to start the synchronization process.

3. **View Log**: Check the specified log file or console output for details about the synchronization operations performed.

## Limitations

### Path Length

Due to Windows filesystem limitations, the fully qualified name for any file or directory, including the path and the filename, must be less than 260 characters. Exceeding this limit can result in errors during synchronization.

### Overcoming Path Length Limitation

Windows 10 version 1607 and newer versions support extended path lengths beyond 260 characters. To enable this feature:

#### Using Group Policy

1. Open the Local Group Policy Editor (`gpedit.msc`).
2. Navigate to `Local Computer Policy` -> `Computer Configuration` -> `Administrative Templates` -> `System` -> `Filesystem`.
3. Double-click the "Enable Win32 long paths" setting, set it to "Enabled", and click OK.

#### Using Registry Editor

1. Open the Registry Editor (`regedit`).
2. Navigate to the following key: `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem`.
3. Find the `LongPathsEnabled` value (or create it as a DWORD value if it doesn't exist).
4. Set `LongPathsEnabled` to `1`.
5. Restart your computer for the changes to take effect.

**Note**: Modifying the registry can have unintended effects. Be cautious and consider backing up the registry before making changes.

## Conclusion

This PowerShell synchronization script, complemented by a user-friendly interface, offers a practical solution for keeping directories in sync. By following the guidelines provided and considering the limitations and workarounds related to path lengths, users can effectively manage and synchronize their file systems.
